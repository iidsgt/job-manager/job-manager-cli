import os
import shutil
from setuptools import setup

setup(
    name='job-manager-cli',
    version='0.0.1',
    description='CLI interface for job manager api.',
    url='https://gitlab.com/iidsgt/job-manager/job-manager-cli.git',
    author_email="evan.clark.professional@gmail.com",
    author='Evan Clark',
    license='Apache 2.0',
    install_requires=['requests']
    scripts=['bin/jobtool.py']
)