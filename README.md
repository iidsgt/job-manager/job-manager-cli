# Job-Manager-CLI

Job-Manager-CLI is a Python library for dealing job manager.

## Installation

Install using pip

```bash
pip install git+https://gitlab.com/iidsgt/job-manager/job-manager-cli.git  
```

```bash
git clone https://gitlab.com/iidsgt/job-manager/job-manager-cli.git
```

## Usage

```bash
python3 jobtool.py submit --cwl <cwl-file> --inputs <inputs-file>
```

You will be prompted to setup a config file, include the base url of the Job Manger when prompted.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
